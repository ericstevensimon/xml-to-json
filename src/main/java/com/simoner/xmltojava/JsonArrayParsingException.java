package com.simoner.xmltojava;

public class JsonArrayParsingException extends RuntimeException {

    public JsonArrayParsingException(String string) {
        super(string);
    }
}
