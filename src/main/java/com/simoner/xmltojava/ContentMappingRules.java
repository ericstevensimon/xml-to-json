package com.simoner.xmltojava;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This class will manage changes to JSON content based upon a provided set of rules.
 * It will also pick a particular array of information from the input JSON and return that.
 * This rules to run and array to return may be controlled by the mapping.properties file.
 */
@Component
@ConfigurationProperties("mapping")
public class ContentMappingRules {

    private List<String> xmlLevelsToStrip;
    private List<FieldMappingRule> rules;

    //Would prefer to use a non-default constructor in this class and remove the setters, but Spring struggles
    //with injection of Configuration Properties via constructor.
    public ContentMappingRules() {
    }

    public ContentMappingRules(List<String> xmlLevelsToStrip,
                               List<FieldMappingRule> rules) {
        this.xmlLevelsToStrip = xmlLevelsToStrip;
        this.rules = rules;
    }

    public void executeFieldMappingRulesOnJsonObject(CustomJsonObject inputJson) {

        for (FieldMappingRule rule : rules) {
            rule.mapContent(inputJson);
        }
    }

    public JSONArray stripLevels(CustomJsonObject inputJson) {
        //The desired result is a base JSON array, so this logic will filter the resulting JSON down to
        //the desired array.
        for (String levelToStrip : xmlLevelsToStrip) {
            if (inputJson.getValue(levelToStrip) instanceof JSONObject) {
                inputJson = inputJson.getValueAsJsonObject(levelToStrip);
            } else if (inputJson.getValue(levelToStrip) instanceof JSONArray) {
                return inputJson.getValueAsJsonArray(levelToStrip);
            } else {
                throw new JsonArrayParsingException(String.format("The provided parameter of %s was neither a JSON array or JSON object", levelToStrip));
            }
        }

        throw new JsonArrayParsingException("An array was not found in the provided path");
    }

    public void setXmlLevelsToStrip(List<String> xmlLevelsToStrip) {
        this.xmlLevelsToStrip = xmlLevelsToStrip;
    }

    public void setRules(List<FieldMappingRule> rules) {
        this.rules = rules;
    }
}
