package com.simoner.xmltojava;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:mappings.properties")
public class AppStartup {

    private static final String PROVIDED_XML = """
            <?xml version="1.0" encoding="UTF-8"?>
            <patients>
              <patient>
                   <id>1234</id>
                   <gender>m</gender>
                   <name>John Smith</name>
                   <state>Michigan</state>
                   <dateOfBirth>03/04/1962</dateOfBirth >
              </patient>
              <patient>
                   <id>5678</id>
                   <gender>f</gender>
                   <name>Jane Smith</name>
                   <state>Ohio</state>
                   <dateOfBirth>08/24/1971</dateOfBirth>
              </patient>
            </patients>
        """;

    private static final String EXPECTED_JSON = """
            [
                   {
                          "patientid": 1234,
                          "sex": "male",
                          "state": "MI",
                          "name": "John Smith",
                          "age": 58
                   },
                   {
                          "patientid": 5678,
                          "sex": "female",
                          "state": "OH",
                          "name": "Jane Smith",
                          "age": 49
                   }
            ]
            """;

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(AppStartup.class, args);

        //Kicking off the conversion from startup as a matter of convenience.  Typically
        //the XML to parse will be dynamic and passed from elsewhere.  Also printing the output to the console
        //for convenience and doing an assertion on the correct format.
        Converter converter = applicationContext.getBean(Converter.class);
        String resultingJson = converter.convertXmlToJson(PROVIDED_XML);
        System.out.println(resultingJson);

        JSONAssert.assertEquals(EXPECTED_JSON, resultingJson, JSONCompareMode.STRICT);
    }
}
