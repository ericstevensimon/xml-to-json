package com.simoner.xmltojava;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Defines the set of possible manipulations that may be done on the values of XML/JSON content.
 */
public enum ValueManipulation {

    DATE_TO_AGE,
    GENDER_CHARACTER_TO_WORD,
    STATE_NAME_TO_ABBREVIATION;

    //TODO As this method grows, might modify to delegate to other smaller classes.
    public Object performValueConversion(String inputValue) {

        Object resultingValue;

        switch (this) {
            case DATE_TO_AGE:
                LocalDate birthDate = LocalDate.parse(inputValue, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                resultingValue = ChronoUnit.YEARS.between(birthDate, LocalDate.now());
                break;
            case GENDER_CHARACTER_TO_WORD:
                String genderCode = inputValue.toLowerCase();
                if (genderCode.equals("m")) {
                    resultingValue = "male";
                } else if (genderCode.equals("f")) {
                    resultingValue = "female";
                } else {
                    throw new IllegalArgumentException(String.format("Unsupported gender of %s", inputValue));
                }
                break;
            case STATE_NAME_TO_ABBREVIATION:
                resultingValue = State.valueOfName(inputValue).getAbbreviation();
                break;
            default:
                throw new IllegalArgumentException(String.format("Unsupported value manipulator of %s", this));
        }
        return resultingValue;
    }
}
