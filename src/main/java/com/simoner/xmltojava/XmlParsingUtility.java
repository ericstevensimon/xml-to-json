package com.simoner.xmltojava;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Component;

/**
 * Trying to mock static methods in unit testing is painful.  This class exists to save that headache by
 * exposing non-static methods for deserializing XML.
 */
@Component
public class XmlParsingUtility {

    public CustomJsonObject parseXmlToJson(String xmlAsString) {
        JSONObject jsonObject = XML.toJSONObject(xmlAsString);
        return new CustomJsonObject(jsonObject);
    }
}
