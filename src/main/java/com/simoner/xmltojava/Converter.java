package com.simoner.xmltojava;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
/**
 * This class manages the conversion of XML to JSON as well as initiating changes
 * to structure of the content.
 */
public class Converter {

    private XmlParsingUtility xmlParsingUtility;
    private ContentMappingRules contentMappingRules;

    public Converter(XmlParsingUtility xmlParsingUtility,
                     ContentMappingRules contentMappingRules) {
        this.xmlParsingUtility = xmlParsingUtility;
        this.contentMappingRules = contentMappingRules;
    }

    /**
     * This method will convert the provided JSON to the desired structure.  First, it converts xml to JSON.
     * Next, the names and values of fields are manipulated.
     * Lastly, it will filter out undesired layers of the structure to return it in the desired array-based format.
     */
    public String convertXmlToJson(String xmlAsString) {
        CustomJsonObject parentJsonObject = xmlParsingUtility.parseXmlToJson(xmlAsString);

        //Modify JSON field names and values according to the configured rules.
        //As noted in the Readme, this process currently only supports conversion of fields and values
        //within an object, rather than supporting conversion of values in an array.  As such, this process
        //functions by collecting all JSON Objects then invoking a process to execute the configured rules
        //to manipulate field name and values on each.
        Collection<CustomJsonObject> allJsonObjectsInTree = parentJsonObject.getObjectsInTree();
        for (CustomJsonObject jsonObjectToMap : allJsonObjectsInTree) {
            contentMappingRules.executeFieldMappingRulesOnJsonObject(jsonObjectToMap);
        }

        JSONArray jsonWithContentMapping = contentMappingRules.stripLevels(parentJsonObject);
        return jsonWithContentMapping.toString();
    }
}
