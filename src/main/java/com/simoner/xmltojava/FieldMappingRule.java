package com.simoner.xmltojava;

import java.util.Optional;

public class FieldMappingRule {

    private String name;
    private String fieldName;
    private Optional<String> revisedFieldName;
    private Optional<ValueManipulation> valueManipulation;

    //Would prefer to use a non-default constructor in this class and remove the setters, but Spring struggles
    //with injection of Configuration Properties via constructor.
    public FieldMappingRule() {
        revisedFieldName = Optional.empty();
        valueManipulation = Optional.empty();
    }

    //Providing a non-default constructor for usage by unit tests.  Somewhat odd to take Optional entries
    //in a constructor but not worrying about it for this.
    public FieldMappingRule(String name,
                            String fieldName,
                            Optional<String> revisedFieldName,
                            Optional<ValueManipulation> valueManipulation) {
        this.name = name;
        this.fieldName = fieldName;
        this.revisedFieldName = revisedFieldName;
        this.valueManipulation = valueManipulation;
    }

    public void mapContent(CustomJsonObject jsonObject) {
        if (jsonObject.has(fieldName)) {

            if (valueManipulation.isPresent()) {
                Object newValue = valueManipulation.get().performValueConversion(jsonObject.getValueAsString(fieldName));
                jsonObject.put(fieldName, newValue);
            }

            if (revisedFieldName.isPresent()) {
                jsonObject.changeName(fieldName, revisedFieldName.get());
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setRevisedFieldName(Optional<String> revisedFieldName) {
        this.revisedFieldName = revisedFieldName;
    }

    public void setValueManipulation(Optional<ValueManipulation> valueManipulation) {
        this.valueManipulation = valueManipulation;
    }
}
