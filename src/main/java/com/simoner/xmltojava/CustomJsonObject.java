package com.simoner.xmltojava;

import com.google.common.collect.Sets;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Represents an Object in JSON.
 * Wraps {@link org.json.JSONObject} to provide additional behavior beyond that which org.JSON provides.
 */
public class CustomJsonObject {

    private final JSONObject jsonObject;

    public CustomJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public CustomJsonObject(String json) {
        this.jsonObject = new JSONObject(json);
    }

    public CustomJsonObject getChildObject(String key) {
        return new CustomJsonObject(jsonObject.getJSONObject(key));
    }

    public boolean has(String key) {
        return jsonObject.has(key);
    }

    public Object getValue(String key) {
        return jsonObject.get(key);
    }

    public CustomJsonObject getValueAsJsonObject(String key) {
        return new CustomJsonObject(jsonObject.getJSONObject(key));
    }

    public JSONArray getValueAsJsonArray(String key) {
        return jsonObject.getJSONArray(key);
    }

    public String getValueAsString(String key) {
        return jsonObject.getString(key);
    }

    public void put(String key, Object value) {
        jsonObject.put(key, value);
    }

    public void changeName(String priorName, String newName) {
        Object value = jsonObject.get(priorName);
        jsonObject.put(newName, value);
        jsonObject.remove(priorName);
    }

    public String asJsonString() {
        return jsonObject.toString();
    }

    public String toString() {
        return asJsonString();
    }

    /**
     * This object represents a tree which may contain child JSON objects.  This method searches the tree,
     * returning all child objects plus itself.  Recursion is used. This gets a bit tricky as, to find all children,
     * we need to search through both the tree of all child objects, as well as all arrays.
     */
    public Collection<CustomJsonObject> getObjectsInTree() {

        Collection<CustomJsonObject> objectsInTree = Sets.newHashSet(this);

        for (String key : jsonObject.keySet()) {
            if (jsonObject.get(key) instanceof JSONObject) {
                CustomJsonObject childObject = getChildObject(key);
                objectsInTree.add(childObject);
                objectsInTree.addAll(childObject.getObjectsInTree());
            } else if (jsonObject.get(key) instanceof JSONArray) {
                objectsInTree.addAll(getChildObjects((JSONArray) jsonObject.get(key)));
            }
        }

        return objectsInTree;
    }

    /**
     * Identifies and returns the child JSON objects of a JSONArray
     */
    private Collection<CustomJsonObject> getChildObjects(JSONArray jsonArray) {

        Collection<CustomJsonObject> childObjects = Sets.newHashSet();

        for (Object entryInArray : jsonArray) {
            if (entryInArray instanceof JSONObject) {
                CustomJsonObject customJsonObject = new CustomJsonObject((JSONObject) entryInArray);
                childObjects.add(customJsonObject);
                childObjects.addAll(customJsonObject.getObjectsInTree());
            }

            if (entryInArray instanceof JSONArray) {
                childObjects.addAll(getChildObjects((JSONArray) entryInArray));
            }
        }

        return childObjects;
    }
}
