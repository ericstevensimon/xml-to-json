package com.simoner.xmltojava;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ConverterTest {

    @Mock
    private XmlParsingUtility xmlParsingUtility;

    @Mock
    private ContentMappingRules contentMappingRules;

    private Converter converter;

    @Before
    public void setup() {
        openMocks(this);
        converter = new Converter(xmlParsingUtility, contentMappingRules);
    }

    @Test
    public void testXmlToJson() {
        String inputXml = "someXml";

        CustomJsonObject jsonWithLevelsUnstripped = new CustomJsonObject("""
            {"childArray":["sadf"]}""");

        JSONArray jsonArray = new JSONArray("""
            ["sadf"]""");

        String expectedJsonWithContentChangesAsString = """
            ["sadf"]""";

        when(xmlParsingUtility.parseXmlToJson(inputXml)).thenReturn(jsonWithLevelsUnstripped);
        when(contentMappingRules.stripLevels(jsonWithLevelsUnstripped)).thenReturn(jsonArray);

        JSONAssert.assertEquals(expectedJsonWithContentChangesAsString, converter.convertXmlToJson(inputXml), JSONCompareMode.STRICT);
        verify(contentMappingRules).executeFieldMappingRulesOnJsonObject(jsonWithLevelsUnstripped);
    }
}
