package com.simoner.xmltojava;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValueManipulationTest {

    @Test
    public void testChangeDateToAgeConversion() {
        //TODO Hard coding an age - this will break on or after 03/04
        assertEquals(58L, ValueManipulation.DATE_TO_AGE.performValueConversion("03/04/1962"));
    }

    @Test
    public void testGenderCharacterToWordWithLowerCaseMale() {
        assertEquals("male", ValueManipulation.GENDER_CHARACTER_TO_WORD.performValueConversion("m"));
    }

    @Test
    public void testGenderCharacterToWordWithUpperCaseFemale() {
        assertEquals("female", ValueManipulation.GENDER_CHARACTER_TO_WORD.performValueConversion("F"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenderCharacterToWordWithUnknownGender() {
         ValueManipulation.GENDER_CHARACTER_TO_WORD.performValueConversion("y");
    }

    @Test
    public void testStateNameToAbbreviation() {
        assertEquals("MI", ValueManipulation.STATE_NAME_TO_ABBREVIATION.performValueConversion("Michigan"));


    }

    @Test
    public void testStateNameToAbbreviationWithMixedCaseInput() {
        assertEquals("MI", ValueManipulation.STATE_NAME_TO_ABBREVIATION.performValueConversion("micHigaN"));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testStateNameToAbbreviationWithUnknownState() {
        ValueManipulation.STATE_NAME_TO_ABBREVIATION.performValueConversion("y");
    }
}
