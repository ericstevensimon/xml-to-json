package com.simoner.xmltojava;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomJsonObjectTest {

    @Test
    public void testGetChildrenWhenChainOfObjects() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {"object": {
                    "subObject1" :{"childField":"sadf"}, 
                    "subObject2": {"childField":"asdf"}
                }
            }""");

        assertEquals(4, inputJson.getObjectsInTree().size());
    }

    @Test
    public void testGetChildrenWhenChildObjectsInAnArray() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {"object": [
                    {"childField":"sadf"}, 
                    {"childField":"asdf"}
                ]
            }""");

        assertEquals(3, inputJson.getObjectsInTree().size());
    }

    @Test
    public void testReplaceName() {
        CustomJsonObject json = new CustomJsonObject("""
            {"field":"sadf"}""");

        String expectedResult = """
            {"newField":"sadf"}""";

        json.changeName("field", "newField");
        assertEquals(expectedResult, json.asJsonString());
    }
}
