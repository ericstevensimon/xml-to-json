package com.simoner.xmltojava;

import com.google.common.collect.Lists;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.openMocks;

public class ContentMappingRulesTest {

    @Mock
    private FieldMappingRule rule;

    private ContentMappingRules contentMappingRules;

    @Before
    public void setup() {
        openMocks(this);
    }

    @Test
    public void testStripLevelsAtTheFirstLevel() {

        CustomJsonObject inputJson = new CustomJsonObject("""
            {
                "anArray": []
            }"""
        );
        contentMappingRules = new ContentMappingRules(Lists.newArrayList("anArray"), Lists.newArrayList(rule));
        JSONArray expectedOutput = inputJson.getValueAsJsonArray("anArray");

        assertEquals(expectedOutput, contentMappingRules.stripLevels(inputJson));
    }

    @Test
    public void testStripLevelsWithTheArrayAtTheSecondLevel() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {
                "anObject": {
                    "anArray": []
                }
            }"""
        );
        contentMappingRules = new ContentMappingRules(Lists.newArrayList("anObject", "anArray"), Lists.newArrayList(rule));

        JSONArray expectedOutput = inputJson.getValueAsJsonObject("anObject").getValueAsJsonArray("anArray");

        assertEquals(expectedOutput, contentMappingRules.stripLevels(inputJson));
    }

    @Test(expected = JSONException.class)
    public void testStripLevelsAttemptingToFindArrayInPathWhichDoesntExist() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {
                "anObject": {
                    "anArray": []
                }
            }"""
        );
        contentMappingRules = new ContentMappingRules(Lists.newArrayList("notValid", "anArray"), Lists.newArrayList(rule));

        contentMappingRules.stripLevels(inputJson);
    }

    @Test(expected = JsonArrayParsingException.class)
    public void testStripLevelsAttemptingToFindArrayInPathWhichDoesntEndWithAnArray() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {
                "anObject": {
                    "anArray": []
                }
            }"""
        );
        contentMappingRules = new ContentMappingRules(Lists.newArrayList("anObject"), Lists.newArrayList(rule));


        contentMappingRules.stripLevels(inputJson);
    }

    @Test(expected = JsonArrayParsingException.class)
    public void testStripLevelsAttemptingToFindArrayInPathWhichContainsAJsonField() {
        CustomJsonObject inputJson = new CustomJsonObject("""
            {
                "anObject": {
                    "aField": "aValue",
                    "anArray": []
                }
            }"""
        );
        contentMappingRules = new ContentMappingRules(Lists.newArrayList("anObject", "aField"), Lists.newArrayList(rule));


        contentMappingRules.stripLevels(inputJson);
    }
}
