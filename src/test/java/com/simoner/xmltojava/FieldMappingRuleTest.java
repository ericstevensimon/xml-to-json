package com.simoner.xmltojava;

import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.Optional;

public class FieldMappingRuleTest {

    @Test
    public void testChangeInName() {
        CustomJsonObject inputJson = new CustomJsonObject("""
                {
                    "myName": "myValue"
                }"""
        );

        FieldMappingRule rule = new FieldMappingRule(
                "test",
                "myName",
                Optional.of("outputName"),
                Optional.empty());

        String expectedJson = """
                {
                    "outputName": "myValue"
                }""";

        rule.mapContent(inputJson);
        JSONAssert.assertEquals(expectedJson, inputJson.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void testChangeJustValue() {
        CustomJsonObject inputJson = new CustomJsonObject("""
                {
                    "aDate": "03/04/1962"
                }"""
        );

        FieldMappingRule rule = new FieldMappingRule(
                "test",
                "aDate",
                Optional.empty(),
                Optional.of(ValueManipulation.DATE_TO_AGE));

        //TODO Hard coding an age - this will break in a few months
        String expectedJson = """
                {
                    "aDate": 58
                }""";

        rule.mapContent(inputJson);
        JSONAssert.assertEquals(expectedJson, inputJson.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void testChangeInNameAndValue() {
        CustomJsonObject inputJson = new CustomJsonObject("""
                {
                    "aDate": "03/04/1962"
                }"""
        );

        FieldMappingRule rule = new FieldMappingRule(
                "test",
                "aDate",
                Optional.of("outputName"),
                Optional.of(ValueManipulation.DATE_TO_AGE));

        //TODO Hard coding an age - this will break in a few months
        String expectedJson = """
                {
                    "outputName": 58
                }""";

        rule.mapContent(inputJson);
        JSONAssert.assertEquals(expectedJson, inputJson.toString(), JSONCompareMode.STRICT);
    }
}
