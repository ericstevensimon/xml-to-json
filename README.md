## Approach and thoughts leading to it
My first thought when reading the challenge description was "ugh, XML and JSON parsing".  Over the years, Java's libraries for dealing with these have greatly improved but working with dynamic XML or JSON is still, at least in my experience, painful.

To avoid dealing with existing libraries, I first contemplated using direct text manipulation on either the incoming XML or outgoing JSON.  Then to use libraries for the XML->JSON conversion.  That idea lasted about ten seconds before deciding it would be too difficult to handle edge cases (such as a value of "id" when needing to convert that), and I resigned myself to digging into a library.

Next, I looked into Jackson, the JSON-related library which I am most familiar.  It can handle XML and, with the hooks it provides around serialization and deserialization, could potentially handle the mapping of field name and values.  However, Jackson's strength is mapping of statically defined JSON, and it relies heavily on annotations.  After some investigation into this option, I felt like using the framework to perform a dynamic set of conversions resulted in fighting against the framework.  I believe there is a way to accomplish using Jackson, and doing so has several benefits, but I decided to see what other options were available.

I then turned to "JSON-java", which seemed to have better support for dynamic JSON while still supporting parsing from XML.  After a bit of digging, I identified the framework can generate a set of objects in Java which represent the JSON tree and these objects are manipulatable.  This provides a hook to perform the desired manipulation of structure.  As compared to Jackson, it requires additional traversal of the JSON tree, so I suspect this approach is less performent than what could be accomplished with Jackson.  But, I thought it would be easier to accomplish.  In the absence of info about required scale, throughput, hardware constraints, etc. I chose this option, which would I believed would give me the freest weekend.

Additionally, I opted to utilize Spring Boot and its Configuration Properties capabilities as a convenient means to parse the mapping config.  Possibly overkill, as this is the only feature needed from Spring Boot but, for purposes of this, I am content with overkill.  

## Gaps not addressed
1) As is, values residing directly within arrays can't be manipulated.  For example, an array of dates can't be converted to an array of ages.  It wouldn't be all that difficult to add but, as this wasn't yet asked for, I didn't tackle.
2) The age converter supports a single date format and isn't configurable to support others 

## Final Thoughts
While I believe I have been successful in achieving the desired outcome, I leave the problem with the suspicion that there exists a simpler approach to addressing the problem.  Whether though a different way of thinking about it, different frameworks, or different languages.  With its more dynamic nature and heavy usage of JSON, I wonder if this is a problem which JavaScript would be better suited for.  In an organizational setting, I would have checked with JS-savvy peers before attempting to tackle in Java.

## PS
I corrected the ages in the output file.  It's not 2017, ya know? :)